package com.codingcrayons.aflab.view.aspectfaces;

import com.codingcrayons.aspectfaces.cache.ResourceCache;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.ondemand.DefaultAFGeneratorHandler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.view.facelets.ComponentConfig;

public class AdaptiveAFGeneratorHandler extends DefaultAFGeneratorHandler {

	private static final String DEFAULT_CONFIG = "html";
	//DEVELOPMENT (10 - cache for 10 sec)
	//PRODUCTION (-1 cache forever -1)
	private static final int AF_CACHE = -1;
	private Boolean development = true;
	private Boolean debug = false;

	public AdaptiveAFGeneratorHandler(ComponentConfig config) {
		super(config, AF_CACHE);

		FacesContext ctx = FacesContext.getCurrentInstance();
		Integer afCache = Integer.parseInt(ctx.getExternalContext().getInitParameter("AF.CACHE"));
		if (afCache != null) {
			super.afCacheTime = afCache;
		}
		Boolean dev = Boolean.parseBoolean(ctx.getExternalContext().getInitParameter("AF.DEVELOPMENT"));
		if (dev != null) {
			this.development = dev;
		}
		Boolean debug = Boolean.parseBoolean(ctx.getExternalContext().getInitParameter("AF.DEBUG"));
		if (debug != null) {
			this.debug = debug;
		}
	}

	private String applySettings(List<String> profiles) {
		String calcLayout = null;
		if (layout != null) {
			calcLayout = layout.getValue();
			calcLayout = (String) executeExpressionInElContext(FacesContext.getCurrentInstance()
					.getApplication().getExpressionFactory(), FacesContext.getCurrentInstance()
					.getELContext(), calcLayout);
		}
		return (calcLayout == null) ? null : ("../../layouts/" + calcLayout);
	}

	@Override
	protected InputStream hookCustomInputStream() {
		String role = "ADMIN";
		String[] roles = {
				role
		};
		List<String> profiles = new ArrayList<String>();
		return generateAsIS(classesToInspect, getConfig(), roles, profiles, applySettings(profiles));
	}

	@Override
	protected InputStream createInputStream(String s) {
		try {
			if (development) {
				ResourceCache.getInstance().clear();
			}
			if (debug) {
				System.out.println(s);
			}
			return new ByteArrayInputStream(s.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return viewFragmentExceptionIS(e);
		}
	}

	protected void hookAddToAFContex(Context context) {
		// Example how to add context variable
		// You can put there everything you want
		//context.getVariables().put("foo", "bar");
	}

	@Override
	protected String getConfig() {
		if (configName == null || configName.getValue().isEmpty()) {
			return DEFAULT_CONFIG;
		}
		return configName.getValue();
	}

}
